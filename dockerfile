FROM node:10.15.3
RUN mkdir -p /var/www/nest-antd-pro-blog/
COPY . /var/www/nest-antd-pro-blog/
LABEL maintainer="1986tianxie@sina.com"
# 项目中的package.json中的npm run build和npm run start:prod分别是构建和运行该nest.js程序的命令
WORKDIR /var/www/nest-antd-pro-blog/
RUN npm install && npm run build
WORKDIR /var/www/nest-antd-pro-blog/dist/
EXPOSE 8082
ENTRYPOINT ["node"]
CMD ["main.js"]
